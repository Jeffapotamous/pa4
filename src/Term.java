import java.util.LinkedList;
public class Term implements Comparable<Object>{

	String name;
	int docFrequency;
	int totalFrequency;
	
	public LinkedList<Occurrence> occur_list = new LinkedList<Occurrence>();
	
	//public Term(String name) which stores the word (name) and 
	//initializes the docFrequency to 0.
	public Term(String name){
		this.name = name;
		this.docFrequency = 0;
		this.totalFrequency = 0;
	}
	//public void incFrequency(String document) which increments totalFrequency 
	//and either a) creates a new Occurrence object if there is not one with 
	//document as its docName and increments docFrequency or b) increments the 
	//frequency of Occurrence with document as its docName.
	public void incFrequency(String document){
		//update total frequency because it appeared in a page.
		this.totalFrequency++;
		SearchMethods.binaryOccurrenceAdd(document, occur_list, 0, occur_list.size());
		this.docFrequency = occur_list.size();
	}
	@Override
	public int compareTo(Object o) {
		if(o instanceof Term != true)
			try {
				throw new Exception("not a Term Object");
			} catch (Exception e) {
				e.printStackTrace();
			}
			Term temp = (Term)o;
		return(temp.name.compareTo(this.name));
	}
	public boolean equals(Object o){
		Term temp = (Term)o;
		if(o instanceof Term != true)
			return false;
		else
			return this.compareTo(temp) == 0;
	}
	public String toString(){
		return this.name;
	}
}
