import java.util.ArrayList;
import java.util.Iterator;


public class HashTable_Iterator<Term> implements Iterator<Term> {
	public ArrayList<Term> termQueue = new ArrayList<Term>();
	private int ht_size;
	private HashTable hTable;
	private Term[] termArray;
	
	public HashTable_Iterator(HashTable ht_in) {
		this.hTable = ht_in;
		this.ht_size = ht_in.size();
		setInorder();
	}
	
	private void setInorder(){
		for(int i = 0; i < this.ht_size; i++){
			//System.out.println("i = " + i);
			if(hTable.table[i] == null || hTable.table[i].toString() == "RESERVED"){
				//System.out.println("inside set order. term to add to the queue" + i + "is null");
			} else {
				termQueue.add((Term) hTable.table[i]);
				//System.out.println(hTable.table[i] + " = term added to the queue");
			}
		}	
	}

	@Override
	public boolean hasNext() {
		if(termQueue.isEmpty()){
			return false;
		} else {
			return true;
		}
		
	}
	public boolean isEmpty() {
		if(ht_size > 0){
			return false;
		} else {
			return true; 
		}
		
	}

	@Override
	public Term next() {
		if(!isEmpty()){
			Term temp = termQueue.get(0);
			termQueue.remove(0);
			return temp;
		} else {
			System.out.println("is empty!");
			return null;
		}
		
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}
}
