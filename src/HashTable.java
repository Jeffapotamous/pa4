import java.util.Iterator;


public class HashTable implements TermIndex, Iterable<Term> {

	public final int HASH_TABLE_SIZE;
	public Term[] table; 				//hash table
	private int size = 0; 				// size of the ADT table
	
	public Term[] getTable(){
		return table;
	}
	
	public HashTable(int hash_table_size){
		this.HASH_TABLE_SIZE = hash_table_size;
		table = new Term[HASH_TABLE_SIZE];
	}
	
	//Parameters: filename - the Document Name, newWord, the name of Term to be added.
	@Override
	public void add(String filename, String newWord) {	
		//Make a placeholder Term to add to the hash table, if we really do need to add it
		Term temp = new Term(newWord);
		System.out.println("attempting to add: " + temp.toString());
		//run newWord through the hash function
		String temp_name = newWord.toLowerCase();
		//and get a hashvalue, or a location in the hash table
		int hash_location = (Math.abs(temp_name.hashCode()) % HASH_TABLE_SIZE);
		System.out.println("Hash location of "+ newWord + " " + hash_location);
		
		//Go to the hash location where we would ideally add the newWord
		for(int i = hash_location; i < this.HASH_TABLE_SIZE; i++){
			//check that location to see if it is null
			if(table[i] == null){
				//If the hash value location is null, then add in our item
				table[i] = temp;
				System.out.println(table[i].toString());
				//update the size because we just added something
				this.size++;
				System.out.println(this.size);
				
				//and gtfo this loop
				break;
			} else if (table[i].toString().equals("RESERVED")){
				//If that location is reserved, we can add it
				//reserved is used mostly for get method
				table[i] = temp;
				
				 //Update the size because we just added something
				this.size++;
				
				//And get out of the loop
				break;
			} else if(table[i].toString().equals(temp_name)) {
				//If we find the word is already in hash table add to the frequency
				table[i].incFrequency(filename);
				break;
			}
		}
			
		
		//rehash when the table is getting full
		//next size =  new_size = (2 * current_size) + 1
	}

	@Override
	public void delete(String word) {
		//This is a temporary placeholder HashNode that contains just a RESERVED name
		Term reserved_term = new Term("RESERVED");
		
		//We take the word we're looking to remove, and make the string lower case
		String temp_name = word.toLowerCase();
		//and get the hashcode. This gives us the location it should be.
		int hash_location = (Math.abs(temp_name.hashCode()) % HASH_TABLE_SIZE);
		System.out.println("Hash location of "+ word + " " + hash_location);
		
		//now we need to iterate through the array until we either find it, or we hit the end
		//so we start at has_location, where the result should be if it didn't collide with something
		//and if it is not there, we move from hash_location until the HASH_TABLE_SIZE, checking each location
		for(int i = hash_location; i < this.HASH_TABLE_SIZE; i++){
			if(table[hash_location].toString().equals(word)){
				table[hash_location] = reserved_term;
				this.size--; //When you delete from the table, update the size
				break; //Once we remove it, we break out of this for loop so that we don't remove anything else.
			}
		}
	}
	
	public void print(){
		for(int i = 0; i < HASH_TABLE_SIZE; i++){
			if(table[i] != null){
				System.out.println(table[i].toString());
			}
		}
	}

	@Override
	public Term get(String word, Boolean printP) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int size() {
		// TODO Auto-generated method stub
		return this.HASH_TABLE_SIZE;
	}
	
	public void printTable(){
		HashTable_Iterator<Term> print_iterator = new HashTable_Iterator<Term>(this);
		//System.out.println("print table! queuesize " + print_iterator.termQueue.size());
		for(int i = 0; i <= print_iterator.termQueue.size()+1; i++){
			//System.out.println("this is the size: " + print_iterator.termQueue.size());
			//System.out.println("this is i" + i);
			System.out.println(print_iterator.next().toString());
		}
	}
	
	@Override
	public Iterator<Term> iterator() {
		// TODO Auto-generated method stub
		return new HashTable_Iterator<Term>(this);
	}

}
