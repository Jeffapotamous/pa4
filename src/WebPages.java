import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.xml.soap.Node;

public class WebPages {
	//private static ArrayList<String> finalList = new ArrayList<String>();
	public static ArrayList<Term> termIndex;
	public WebPages()
	{
		termIndex = new ArrayList<Term>();
	}
	// Method that reads in all of the data from the text, adds them to termIndex
	public void addPage(String filename)
	{
		String[] words = null;
		try {
			Scanner input = new Scanner(new File(filename));
			String instring = new String();
			while(input.hasNext())
			{
				instring += input.next() + " ";
			}
			instring = instring.replaceAll("\\<.*?>","");
			instring = instring.replaceAll("(?!\')\\p{Punct}", " ").toLowerCase().trim();
			words = instring.split("\\s+");
				for(int i = 0; i < words.length; i++)
				{
					SearchMethods.binaryTermAdd(words[i], filename, termIndex, 0, termIndex.size());	
				}
			input.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Error: File not found.");
		}
	}
	public void pruneStopWords(int n){
		MergeSort.sort(termIndex, false);
		for(int i = 0; i < n; i++){
			termIndex.remove(termIndex.size()-1);
		}
		MergeSort.sort(termIndex, true);
		System.out.println();
	}
	public String[] whichPages(String word){
		double TFIDF = 0;
		int index = SearchMethods.binarySearch(word, termIndex, 0, termIndex.size());
		String[] result;
		if(index >= 0){
			result = new String [termIndex.get(index).occur_list.size()];
			for(int i = 0; i < result.length; i++){
				result[i] = termIndex.get(index).occur_list.get(i).getName();
			}
			return result;
		}
		else
			return null;
	 }
	//Prints the terms from termIndex
	public void printTerms(){
		System.out.println("WORDS");
		for(int i = 0; i < termIndex.size(); i++)
		{
			System.out.println(termIndex.get(i).name);
		}
		System.out.println();
		//System.out.println("NUMBER OF WORDS: " + termIndex.size());
	}
}
