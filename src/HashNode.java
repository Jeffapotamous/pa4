//This code was taken from Data Abstraction & problem Solving with Java by Prichard & Cerrano pg.748
//and altered by Cooper Heinrichs and Jeffrey Buehler.

public class HashNode<Term> {
	private String name;
	private int key;
	private Term item;
	
	public HashNode(Term newItem){
		this.item = newItem;
		this.name = newItem.toString().toLowerCase(); //hashcoded equals the key
		this.key = Math.abs(this.name.hashCode());
	} //end constructor
	
	public Term getItem(){
		return this.item;
	} 
	
	public int getKey(){
		return key;
	}
}
