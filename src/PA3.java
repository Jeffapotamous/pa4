import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class PA3 {

	public static void main(String args[]){
		HashTable test_table = new HashTable(101);
		test_table.add("document", "Test_Word_one");
		test_table.add("document", "Test_Word_two");
		test_table.add("document", "Test_Word_three");
		test_table.delete("Test_Word_two");
		System.out.println("print table *************");
		System.out.println("hash table size - " + test_table.HASH_TABLE_SIZE);
		System.out.println();
		System.out.println("print without the iterator");
		test_table.print();
		System.out.println();
		System.out.println("print with the iterator");
		test_table.printTable();
		
		/*
		WebPages webpages = new WebPages();
		
		//list of the files to be imported
		ArrayList<String> fileNames = new ArrayList<String>();
		
		//number of stop words to be pruned
		int p_num = 0;
		
		//words for whichPages method
		ArrayList<String>  wp_words = new ArrayList<String>();
		
		try {
			Scanner input = new Scanner(new File(args[0]));
			
			//have we reached the end of the files yet?
			boolean eof = false;
			
			while(input.hasNext()){
				
				String place_holder = new String(input.next());
				//create a string with the file name
				
				
				if(place_holder.equals("*EOFs*")){
					//input.next();
					p_num = input.nextInt();
					eof = true;
					
				} else if(!eof){
					fileNames.add(place_holder);
					
				} else {
					wp_words.add(place_holder);
				}
				
			}
			input.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error: File not found.");
		}
		
		//System.out.println("File Names: " + fileNames.toString());
		//System.out.println("Number of Stop Words to be Pruned: " + p_num);
		//System.out.println("Words for whichPages method: " + wp_words.toString());
		
		//once all the variables are filled, add the pages
		for(int i = 0; i < fileNames.size(); i++){
			webpages.addPage(fileNames.get(i));
		}
		
		//print out the words in the same format as P1
		webpages.printTerms();
		
		//pruneStopWords
		webpages.pruneStopWords(p_num);
		
		//print out the words in the same format as P1 
		//this time without the stop words
		webpages.printTerms();
		
		//run whichPages method on webPages for each word in the
		//input file
		for(int i = 0; i < wp_words.size(); i++){
			String[] s = webpages.whichPages(wp_words.get(i));
			//System.out.println("wp_word we're looking for: " + wp_words.get(i));
			if(s!= null){
				System.out.print(wp_words.get(i) + " in pages: ");
				for(int j = 0; j < s.length-1; j++)
					System.out.print(s[j] + ", ");
				System.out.print(s[s.length-1]);
				System.out.println();
			}
			else
			{
				System.out.print( wp_words.get(i) + " not found");
				System.out.println();
			}
		}*/
	}
}
