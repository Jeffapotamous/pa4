import java.util.ArrayList;
import java.util.Iterator;

@SuppressWarnings("hiding")
public class InorderIterator<Term> implements Iterator<Term> {
	ArrayList<Term> arrayListNodes = new ArrayList<Term>();
	private BST binTree;
	public InorderIterator(BST binTree){
		this.binTree = binTree; 
		setInorder();
	}
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return false;
	}
	//Return the next item in the list starting at the back.
	@Override
	public Term next() {
		Term temp = arrayListNodes.get(arrayListNodes.size()-1);
		arrayListNodes.remove(arrayListNodes.size()-1);
		return temp;
	}
	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}
	@SuppressWarnings("unchecked")
	//sort recursively through the tree and add to the array list. 
	private void setInorder(){
		inorder((TreeNode<Term>) this.binTree.getRoot());
	}
	private void inorder(TreeNode<Term> root){
		if(root.getLeft() != null)
			inorder(root.getLeft());
		
		this.arrayListNodes.add(root.getItem());
		
		if(root.getRight() != null)
			inorder(root.getRight());
	}

}
