public class Occurrence {
	private String docName;
	private int termFrequency;
	public Occurrence(String name)
	{
		docName = name;
		termFrequency = 0;
	}
	//Increment the term frequency
	public void inFrequency(){
		termFrequency++;
	}
	//get the name
	public String getName(){
		return docName;
	}
	//get the frequency
	public int getFrequency(){
		return termFrequency;
	}

}
