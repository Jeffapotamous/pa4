import java.util.ArrayList;
import java.util.LinkedList;
public class SearchMethods {
	//Method that searches a LinkedList of Occurrences and adds the term if it is not already in the list, otherwise increments the frequency of the Occurrence.
	static public void binaryOccurrenceAdd(String key, LinkedList<Occurrence> a, int left, int right) {
        if (right <= left){
        	Occurrence o = new Occurrence(key);
        	a.add(left, o);
        }
        int middle = left + (right - left) / 2;
        int compare = a.get(middle).getName().compareToIgnoreCase(key);
        if (compare > 0) 
        	binaryOccurrenceAdd(key, a, left, middle);
        else if (compare < 0) 
        	binaryOccurrenceAdd(key, a, middle+1, right);
        else              
        	a.get(middle).inFrequency();
    }
	//Method that searches an ArrayList and adds the term if it is not already in the list, otherwise increments the frequency of the term.
	static public void binaryTermAdd(String key, String document, ArrayList<Term> a, int left, int right) {
        if (right <= left){
        	Term t = new Term(key);
        	//t.incFrequency(document);
        	a.add(left, t);
        }
        int middle = left + (right - left) / 2;
        int compare = a.get(middle).name.compareToIgnoreCase(key);
        if (compare > 0)
        	binaryTermAdd(key, document, a, left, middle);
        else if (compare < 0) 
        	binaryTermAdd(key, document, a, middle+1, right);
        else             
        	a.get(middle).incFrequency(document);//Return -1 if the item is already in the list.
    }
	//Special binary search method that returns -1 when an item is in the list and returns the index at which an item belongs if it is not.
	static public int binarySearchReverse(String key, ArrayList<Term> a, int left, int right) {
        if (right <= left) 
        	return left;//Return the index to ad the item at if it is not in the list.
        int middle = left + (right - left) / 2;
        int compare = a.get(middle).name.compareToIgnoreCase(key);
        if (compare > 0) 
        	return binarySearchReverse(key, a, left, middle);
        else if (compare < 0) 
        	return binarySearchReverse(key, a, middle+1, right);
        else              
        	return -1;//Return -1 if the item is already in the list.
    }
	//Binary search method that returns the location of an item or returns -1 if the item is not in the list.
	static public int binarySearch(String key, ArrayList<Term> a, int left, int right) {
        if (right <= left) 
        	return -1;//return -1 if key is not in the list
        int middle = left + (right - left) / 2;
        int compare = a.get(middle).name.compareToIgnoreCase(key);
        if (compare > 0) 
        	return binarySearch(key, a, left, middle);
        else if (compare < 0) 
        	return binarySearch(key, a, middle+1, right);
        else              
        	return middle;//return the location of key
    }
	//Binary search method that returns the location of an item or returns -1 if the item is not in the list.
	static public int binarySearch(String key, LinkedList<Occurrence> a, int left, int right){
		 if (right <= left) 
	        	return -1;//return -1 if key is not in the list
	        int middle = left + (right - left) / 2;
	        int compare = a.get(middle).getName().compareToIgnoreCase(key);
	        if      (compare > 0) 
	        	return binarySearch(key, a, left, middle);
	        else if (compare < 0) 
	        	return binarySearch(key, a, middle+1, right);
	        else              
	        	return middle;//return the location of key
	}
}
