import java.util.ArrayList;
public class MergeSort {
private static ArrayList<Term> numbers;
private static ArrayList<Term> helper;
private static int number;
private static boolean alphabetical;
private static int counter;
  public static void sort(ArrayList<Term> values,boolean alphabetical) {
	counter = 0;
    numbers = values;
    number = values.size();
    helper = new ArrayList<Term>();
    MergeSort.alphabetical = alphabetical;
    mergesort(0, number - 1);
    System.out.println("Copies: " + counter);
  }
  private static void mergesort(int low, int high) {
    //Check that the ArrayList is not already sorted, if not split it into two pieces, sort each piece and merge them back together.
    if (low < high) {
      int middle = low + (high - low) / 2;
      mergesort(low, middle);
      mergesort(middle + 1, high);
      merge(low, middle, high);
    }
  }
  private static void merge(int low, int middle, int high) {
    // Copy both pieces to the helper
    for (int i = low; i <= high; i++) {
    	helper.add(i, numbers.get(i));
    }
    int i = low;
    int j = middle + 1;
    int k = low;
    if(alphabetical)
    {
    	while (i <= middle && j <= high) 
    	{
    		//compare terms on each side, also increment counter
    		if (helper.get(i).name.compareTo(helper.get(j).name) <= 0) {
    			counter++;
    			numbers.set(k, helper.get(i));
    			i++;
    		} else {
    			numbers.set(k, helper.get(j));
    			j++;
    		}
    		k++;
    	}
    }
    else
    {
    	while (i <= middle && j <= high) 
    	{
    		//compare terms on each side, also increment counter
    		if (helper.get(i).totalFrequency <= helper.get(j).totalFrequency) {
    			counter++;
    			numbers.set(k, helper.get(i));
    			i++;
    		} else {
    			numbers.set(k, helper.get(j));
    			j++;
    		}
    		k++;
    	}	
    }
    // Copy to the final ArrayList
    while (i <= middle) {
    	numbers.set(k, helper.get(i));
      k++;
      i++;
    }
  }
} 