import java.util.Iterator;
public class BST implements Iterable<Term>{
	public TreeNode<Term> root;
	private int count;
	public BST(){
		root = null;
		count = 0;
	}
	//get the number of words in document.
	public int size(){
		return count;
	}
	//get a term
	public Term get(String word, Boolean printDepth){
		int depth = 0;
		Term temp = new Term(word);
		if(root == null)
			return null;
		else if(root.getItem().compareTo(temp) > 0)
			return get(temp, printDepth,depth++, root.getLeft());
		else if(root.getItem().compareTo(temp) < 0)
			return get(temp, printDepth,depth++, root.getRight());
		else
			return (Term) root.getItem();		
	}
	//recursive helper method
	public Term get(Term temp, Boolean printDepth, int depth, TreeNode<Term> current){
		if(current == null)
			return null;
		else if(current.getItem().compareTo(temp) > 0)
			return get(temp, printDepth,++depth, current.getLeft());
		else if(current.getItem().compareTo(temp) < 0)
			return get(temp, printDepth,++depth, current.getRight());
		else{
			if(printDepth)
				System.out.println(depth);
			return (Term) current.getItem();
		}
		
	}
	public void add(String documentName, String word ){
		Term temp = new Term(word);
		System.out.println(temp);
		if(root == null)
			root = new TreeNode<Term>(temp);
		else
			add(documentName, root, temp);
		/*else if(root.getItem().compareTo(temp) > 0){
			add(documentName, root.getLeft(), temp);
		}
		else if(root.getItem().compareTo(temp) < 0){
			add(documentName, root.getRight(), temp);	
		}*/		
	}
	public void add(String documentName, TreeNode<Term> current, Term temp){
		if(current.getLeft() == null && current.getItem().compareTo(temp) > 0){
			current.setLeft(new TreeNode<Term>(temp));
		}
		else if(current.getRight() == null && current.getItem().compareTo(temp) < 0){
			current.setRight(new TreeNode<Term>(temp));
		}
		else if(current.getLeft() != null && current.getItem().compareTo(temp) > 0){
			add(documentName, current.getLeft(), temp);
		}
		else if(current.getRight() != null && current.getItem().compareTo(temp) < 0){
			add(documentName, current.getRight(), temp);
		}
		else{
			current.getItem().incFrequency(documentName);
		}	
	}
	public void delete(String word){
		
	}
	//get the root.
	public TreeNode<Term> getRoot(){
		return root;
	}
	@Override
	public Iterator<Term> iterator() {
		// TODO Auto-generated method stub
		return new InorderIterator<Term>(this);
	}

}
